const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// MongoDB connection
mongoose.connect("mongodb+srv://admin2:admin2@wdc028-course-booking.ywk7a.mongodb.net/?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
})
/*mongoose.connect("mongodb+srv://jeniezuitt:admin@cluster0.vsejk.mongodb.net/b177-to-do?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
})*/


// Set notification for connection success or failure
// Connection to the database
let db = mongoose.connection;

// If a connection error occured, output a message in the console
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output a message in the console
db.once("open", () => console.log("We're connected to the cloud database"));

// Create a Task schema

const taskSchema = new mongoose.Schema({
	name : String,
	status: {
		type: String,
		// Default values are the predefined values for a field
		default: "pending"
	}
})

// Create Models
// Server > Schema > Database > Collection (MongoDB)
const Task = mongoose.model("Task", taskSchema);

app.use(express.json());

// Create a POST route to create a new task
app.post("/tasks", (req, res) => {
	Task.findOne({name : req.body.name}, (err, result) => {
		// If a document was found and the document's name matches the information sent via the client/postman
		if(result != null && result.name == req.body.name){
			// Returns a message to the client/postman
			return res.send("Duplicate task found")
		}
		// If no document found
		else{
			// Create a new task and save it to the database
			let newTask = new Task({
				name : req.body.name
			})

			newTask.save((saveErr, savedTask) => {
				// If there are errors in saving
				if(saveErr){
					return console.error(saveErr);
				}
				// If no error found while creating the document
				else{
					return res.status(201).send("New task created");
				}
			})
		}
	})
})
// Create a GET request to retreive all the tasks
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		// If an error occured
		if(err){
			// Will print any errors found in the console
			return console.log(err);
		}
		// if no errors found
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})


app.use(express.urlencoded({extended:true}));

app.listen(port, () => console.log(`Server running at port ${port}`));
