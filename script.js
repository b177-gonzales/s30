const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// MongoDB connection
mongoose.connect("mongodb+srv://admin2:admin2@wdc028-course-booking.ywk7a.mongodb.net/?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection;

app.use(express.json());

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"));

// Create a User schema

const userSchema = new mongoose.Schema({
	name : String,
	status: {
		type: String,
		// Default values are the predefined values for a field
		default: "pending"
	}
})

const User = mongoose.model("User", userSchema);

let users = [];

// Create a POST 
app.post("/signup", (req, res) => {
	console.log(req.body);

	if(req.body.username !== '' && req.body.password !== ''){
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered`);
	}
	else{
		res.send("Please input BOTH username and password.");
	}
})

app.listen(port, () => console.log(`Server running at port ${port}`));
